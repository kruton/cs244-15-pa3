JFLAGS = -g
JC = javac
JARPATH = lib/*
LP = -Djava.library.path
LIBPATH = rocksaw-1.0.3/lib/
MAIN = ccs.neu.edu.andang.OptAckAttack
CP = -classpath
CC = gcc
SERVERNAME = victim-server

.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) -classpath "${CLASSPATH}:src:lib/*" $*.java

CLASSES = \
        ./src/ccs/neu/edu/andang/OptAckAttack.java \

SERVER_OBJS = tcpserver/server.o

default: $(SERVERNAME) classes

classes: $(CLASSES:.java=.class)

clean:
	@$(RM) -f $(SERVERNAME) $(SERVER_OBJS)
	@find src -name *.class -print | xargs rm -f

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

victim-server: $(SERVER_OBJS)
	$(CC) -o $@ $^ $(CFLAGS)

