#!/usr/bin/env python
'''
Plot attacks against various numbers of victims at various window sizes.
From Figure 3 in the Opt-Ack paper.
'''
from helper import *
import plot_defaults

from itertools import izip_longest, cycle
from collections import defaultdict, OrderedDict

from matplotlib.ticker import MaxNLocator
from pylab import figure

parser = argparse.ArgumentParser()
parser.add_argument('--templates',
                    help="Templates of the files to parse",
                    required=True,
                    action="store",
                    nargs='+')

parser.add_argument('--sizes', '-s',
                    help="Status output files to plot",
                    required=True,
                    action="store",
                    nargs='+')

parser.add_argument('--names',
                    help="Names of the different lines to plot",
                    required=True,
                    action="store",
                    nargs='+')

parser.add_argument('--sample-rate',
                    dest="sample_rate",
                    type=int,
                    help="Number of samples to take per second",
                    default=1)

parser.add_argument('--out', '-o',
                    help="Output png file for the plot.",
                    default=None) # Will show the plot

args = parser.parse_args()

def parse_status(fname):
    ret = []
    lines = open(fname).readlines()
    for line in lines:
        if 'status: ' not in line:
            continue
        try:
            (_, milliseconds, bytes) = line.split(' ')
            ret.append([float(milliseconds)/1000.0, int(bytes)])
        except:
            print 'wtf mate'
            break
    return ret

# This makes it easier to read the lines
lines = ["-","--",":","-."]
linecycler = cycle(lines)

m.rc('figure', figsize=(16, 8))
fig = figure()
ax = fig.add_subplot(111)
ax.set_yscale('log')
ax.set_xscale('log')

for i, template in enumerate(args.templates):
    print 'Plotting {name}'.format(name=args.names[i])
    max_attack_num_victims = []
    max_attack_bytes_second = []
    for size in args.sizes:
        total_bytes_sec = defaultdict(int)
        for n in xrange(int(size)):
            f = template.format(total=size, number=n)
            data = parse_status(f)
            if not data:
                continue
            for t, b in data:
                total_bytes_sec[t] += b

        times = sorted(total_bytes_sec.keys())
        trimmed_total_bytes_sec = [total_bytes_sec[k] for k in times]

        max_attack = max([y - x for x, y in zip(trimmed_total_bytes_sec[:-1], trimmed_total_bytes_sec[1:])])

        max_attack_num_victims.append(size)
        max_attack_bytes_second.append(max_attack)

    ax.plot(max_attack_num_victims, max_attack_bytes_second, next(linecycler), lw=2, label=args.names[i])

# Print out the line legend
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

plt.ylabel("Bytes/second")
plt.xlabel("Number of Victims")
plt.grid(False)

if args.out:
    plt.savefig(args.out)
else:
    plt.show()
