#!/usr/bin/python

"CS244 Assignment 3: OptAck Attack"

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg
from mininet.util import dumpNodeConnections
from mininet.cli import CLI # Needed only for debug!

from functools import partial
from subprocess import Popen
from time import sleep, time
import termcolor as T
from argparse import ArgumentParser

import atexit
import os


def cprint(s, color, cr=True):
    """Print in color
       s: string to print
       color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),


# Parse arguments

parser = ArgumentParser(description="OptAck tests")
parser.add_argument('--bw-attacker', '-B',
                    dest="bw_attacker",
                    type=float,
                    action="store",
                    help="Bandwidth of host links",
                    required=True)

parser.add_argument('--bw-victim', '-b',
                    dest="bw_victim",
                    type=float,
                    action="store",
                    help="Bandwidth of network link",
                    required=True)

parser.add_argument('--delay',
                    dest="delay",
                    type=float,
                    help="Delay in milliseconds of attacker link",
                    default=10)

parser.add_argument('--dir', '-d',
                    dest="dir",
                    action="store",
                    help="Directory to store outputs",
                    default="results",
                    required=True)

parser.add_argument('--mss',
                    dest="mss",
                    type=int,
                    action="store",
                    help="Maximum segment size",
                    default=1460)

parser.add_argument('--wscale',
                    dest="wscale",
                    type=int,
                    action="store",
                    help="Window scaling factor",
                    default=4)

parser.add_argument('-n',
                    dest="n",
                    type=int,
                    action="store",
                    help="Number of nodes in star.  Must be >= 3",
                    required=True)

parser.add_argument('--cong',
                    dest="cong",
                    help="Congestion control algorithm to use",
                    default="reno")

parser.add_argument('--attack-cmd',
                    dest="attack_cmd",
                    help="Path to custom attack command",
                    required=True)

parser.add_argument('--server-cmd',
                    dest="server_cmd",
                    help="Path to custom server command",
                    required=True)

parser.add_argument('--sample-rate',
                    dest="sample_rate",
                    type=int,
                    help="Number of samples to take per second",
                    default=1)

parser.add_argument('--run-time',
                    dest="run_time",
                    type=int,
                    help="Duration to run the experiment",
                    default=35)

# Expt parameters
args = parser.parse_args()

CUSTOM_SERVER_CMD = args.server_cmd
assert(os.path.exists(CUSTOM_SERVER_CMD))
CUSTOM_ATTACK_CMD = args.attack_cmd
#assert(os.path.exists(CUSTOM_ATTACK_CMD))

if not os.path.exists(args.dir):
    os.makedirs(args.dir)

lg.setLogLevel('info')

# Topology to be instantiated in Mininet
class OptAckTopo(Topo):
    "Topology for Opt-Ack attack."

    def build(self, n=2, bw_victim=None, bw_attacker=None, delay=None):
        # Attacker
        attacker = self.addHost('attacker')

        # Switch to connect them all
        switch = self.addSwitch('s0')

        # Set up the various classes to use for connection params
        victimLink = partial(TCLink, bw=bw_victim)
        attackerLink = partial(TCLink, delay=delay, bw=bw_attacker)

        # Connect the attacker first
        self.addLink(attacker, switch, cls=attackerLink)

        # Now add all the victims
        for i in xrange(n):
            # Create the victim
            victim = self.addHost('victim%s' % i)

            # Connect it up!
            self.addLink(victim, switch, cls=victimLink)

def median(l):
    "Compute median from an unsorted list of values"
    s = sorted(l)
    if len(s) % 2 == 1:
        return s[(len(l) + 1) / 2 - 1]
    else:
        lower = s[len(l) / 2 - 1]
        upper = s[len(l) / 2]
        return float(lower + upper) / 2

def format_floats(lst):
    "Format list of floats to three decimal places"
    return ', '.join(['%.3f' % f for f in lst])

def ok(fraction):
    "Fraction is OK if it is >= args.target"
    return fraction >= args.target

def format_fraction(fraction):
    "Format and colorize fraction"
    if ok(fraction):
        return T.colored('%.3f' % fraction, 'green')
    return T.colored('%.3f' % fraction, 'red', attrs=["bold"])

def get_all_victims(net):
    return [host for host in net.hosts if host.name.startswith('victim')]

def start_receivers(net):
    """Starts servers on each of the victims."""
    victims = get_all_victims(net)
    for victim in victims:
        output_file = 'victim_n%d_%s.txt' % (args.n, victim.name)
        victim.popen('{cmd} 32001 {rate} > {dir}/{file}'.format(
            cmd=CUSTOM_SERVER_CMD, rate=args.sample_rate, dir=args.dir, file=output_file),
            shell=True)

@atexit.register
def stop_attacks():
    """Manually kill the attack processes."""
    Popen("pgrep java | xargs kill -9", shell=True).wait()

@atexit.register
def stop_server():
    """Manually kill the server process."""
    Popen("pgrep %s | xargs kill -9" % (os.path.basename(CUSTOM_SERVER_CMD)), shell=True).wait()

def start_attack(net):
    # Seconds to run iperf; keep this very high
    attacker = net.get('attacker')
    # Prevent the kernel from spoiling our fun!
    attacker.popen("iptables -t filter -I OUTPUT -p tcp --dport %d --tcp-flags RST RST -j DROP" % 32001,
            shell=True)
    
    attack_config = '%s/attack-config.txt' % args.dir
    with open(attack_config, 'w') as f:
        victims = get_all_victims(net)
        for victim in victims:
            f.write('%s %d %d %d %f\n' % (victim.IP(), 32001, args.mss, args.wscale, 2*args.delay))
        output_file = 'attacker-log.txt'
    attacker.popen('%s %s %s > %s/%s' % (CUSTOM_ATTACK_CMD, attack_config, args.dir,
        args.dir, output_file), shell=True)

def main():
    "Create network and run OptAck experiment"

    start = time()
    # Reset to known state
    topo = OptAckTopo(n=args.n, bw_attacker=args.bw_attacker,
                      delay='%sms' % args.delay,
                      bw_victim=args.bw_victim)
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    dumpNodeConnections(net.hosts)
    #net.pingAll()

    start_receivers(net)

    # For debugging before the experiment
    #CLI(net)

    cprint("Starting experiment; will run for {time} seconds...".format(time=args.run_time), "green")
    start_attack(net)

    sleep(args.run_time)

    # For debugging after the experiment
    #CLI(net)

    net.stop()
    Popen("killall -9 top bwm-ng tcpdump cat mnexec", shell=True).wait()
    end = time()
    cprint("OptAck attack ran for %.3f seconds" % (end - start), "yellow")

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf; mn -c")

