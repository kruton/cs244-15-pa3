#!/usr/bin/env python
'''
Plot attacks against various numbers of victims. From Figure 2 in the
Opt-Ack paper.
'''
from helper import *
import plot_defaults

from itertools import izip_longest, cycle
from collections import defaultdict, OrderedDict

from matplotlib.ticker import MaxNLocator
from pylab import figure

parser = argparse.ArgumentParser()
parser.add_argument('--template',
                    help="Template of the files to parse",
                    required=True,
                    action="store")

parser.add_argument('--sizes', '-s',
                    help="Status output files to plot",
                    required=True,
                    action="store",
                    nargs='+')

parser.add_argument('--sample-rate',
                    dest="sample_rate",
                    type=int,
                    help="Number of samples to take per second",
                    default=1)

parser.add_argument('--time', '-t',
                    help="Number of seconds the test ran for",
                    type=int)

parser.add_argument('--out', '-o',
                    help="Output png file for the plot.",
                    default=None) # Will show the plot

args = parser.parse_args()

def parse_status(fname):
    ret = []
    lines = open(fname).readlines()
    for line in lines:
        if 'status: ' not in line:
            continue
        try:
            (_, milliseconds, bytes) = line.split(' ')
            ret.append([float(milliseconds)/1000.0, int(bytes)])
        except:
            print 'wtf mate'
            break
    return ret

# This makes it easier to read the lines
lines = ["-","--",":","-."]
linecycler = cycle(lines)

m.rc('figure', figsize=(16, 8))
fig = figure()
ax = fig.add_subplot(111)
ax.set_yscale('log')
for i, size in enumerate(args.sizes):
    total_bytes_sec = defaultdict(int)
    for n in xrange(int(size)):
        f = args.template.format(total=size, number=n)
        data = parse_status(f)
        if not data:
            continue
        for t, b in data:
            total_bytes_sec[t] += b

    xaxis = sorted(total_bytes_sec.keys())[0:args.sample_rate*args.time]
    trimmed_total_bytes_sec = [total_bytes_sec[k] for k in xaxis]

    start_time = xaxis[0]
    xaxis = [t - start_time for t in xaxis]
    yaxis = [y - x for x, y in zip([0] + trimmed_total_bytes_sec[0:-1], trimmed_total_bytes_sec)]

    ax.plot(xaxis, yaxis, next(linecycler), lw=2, label='%s victims' % size)
    ax.xaxis.set_major_locator(MaxNLocator(4))
    if args.time is not None:
        ax.set_xlim((0, args.time))

# Print out the line legend
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

plt.ylabel("Bytes/s (log scale)")
plt.xlabel("Seconds")
plt.grid(True)

if args.out:
    plt.savefig(args.out)
else:
    plt.show()
