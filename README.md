# CS244 '15 PA3
A recreation of the [Misbehaving TCP Receivers Can Cause Internet-Wide
Congestion Collapse][optack-paper] paper. This implements the attack
described in Figure 2 and 3.

### Installation

We recommend using Ubuntu 12.01 or later for running this experiment
because a build of mininet is readily available in the distribution's
default package repositories.

To install this on an Ubuntu-based system, simply follow these
steps:

```sh
sudo apt-get update
sudo apt-get install mininet openjdk-7-jdk
git clone https://bitbucket.org/kruton/cs244-15-pa3.git
```

### Running on Amazon EC2

You can use our pre-made instance of the Opt-Ack Attack in Java
project on Amazon EC2 if you use AMI instance id `ami-f397a8c3`.
Follow the [instructions for Finding a Linux AMI][finding-ami]
on Amazon's website.

### Running experiment

```sh
cd cs244-15-pa3
sudo ./run.sh
```

Output files will be `figure2.png` and `figure3.png`.

[optack-paper]:https://www.cs.umd.edu/~capveg/optack/optack-extended.pdf
[finding-ami]:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html
