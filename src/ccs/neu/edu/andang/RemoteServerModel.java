package ccs.neu.edu.andang;

import static com.esotericsoftware.minlog.Log.*;

public class RemoteServerModel {
    /** Maximum segment size in bytes. */
    private long mss;

    /** Congestion window in number of packets. */
    private double cwnd;

    /** Slow-start threshhold in number of packets. */
    private int ssthresh;

    /** Max receiver window. */
    private long maxWindow;

    public RemoteServerModel(long mss, long maxWindow) {
        this.mss = mss;
        this.maxWindow = maxWindow;
        cwnd = 1.0;
        ssthresh = 15;
    }

    /** Called whenever the remote should have received an ack. */
    public void receivedAck() {
        if (cwnd < ssthresh) {
            cwnd += 1.0;
        } else {
            cwnd += 1.0 / cwnd;
        }
        debug("cwnd is now " + cwnd);
    }

    /** Returns congestion window in bytes. */
    public long getEstimatedCongestionWindow() {
        debug("maxWindow=" + maxWindow + ", cwnd*mss=" + (cwnd * mss));
        return Math.min(maxWindow, (long) Math.floor((long) (cwnd * mss)));
    }
}
