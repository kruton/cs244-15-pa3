#!/usr/bin/env bash

# Note: Mininet must be run as root.  So invoke this shell script
# using sudo.

# Sizes to run for experiment for Figure 2.
sizes="1 2 4 8 16 32 64 128 256 512"

# Seconds to run the OptAck attack
time=40

# Bandwidth for attacker. Equivalent of T1.
bwattacker=1.544

# Bandwidth of victim. 100Mbps
bwvictim=100

# attacker -> switch -> victim -> switch -> attacker
# 2 hops of 5ms latency is 10ms overall latency for RTT
delay=5

# Number of samples to take per second. The paper used 1/second.
sample_rate=1


dir="attack-`date +%s`"

curdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Exit on error
set -e

javabin="$(which java)"
if [ ! -x ${javabin} ]; then \
    echo "You must install Java"
    echo "Run:"
    echo "   sudo apt-get install openjdk-7-jdk"
    exit 1
fi

if ! which mn > /dev/null 2>&1; then \
    echo "You must install mininet"
    echo "Run:"
    echo "   sudo apt-get install mininet"
    exit 1
fi

make


## Figure 2

f2_dir="${dir}-f2"
for n in ${sizes}; do \
    python optackattack.py -n ${n} --bw-attacker=$bwattacker --bw-victim=$bwvictim --delay=$delay --dir=${f2_dir} --attack-cmd="java -classpath ${curdir}/src:${curdir}/lib/* -Djava.library.path=${curdir}/rocksaw-1.0.3/lib/ ccs.neu.edu.andang.OptAckAttack" --server-cmd "${curdir}/victim-server" --sample-rate "${sample_rate}" --run-time ${time}
done

python plot_figure2.py --template "${f2_dir}/victim_n{total}_victim{number}.txt" -s ${sizes} -o figure2.png -t 30 --sample-rate "${sample_rate}"


## Figure 3

f3_mss88_dir="${dir}-f3-mss88"
for n in ${sizes}; do \
    python optackattack.py -n ${n} --bw-attacker=$bwattacker --bw-victim=$bwvictim --delay=$delay --dir=${f3_mss88_dir} --attack-cmd="java -classpath ${curdir}/src:${curdir}/lib/* -Djava.library.path=${curdir}/rocksaw-1.0.3/lib/ ccs.neu.edu.andang.OptAckAttack" --server-cmd "${curdir}/victim-server" --sample-rate "${sample_rate}" --run-time ${time} --wscale 0 --mss 88
done

f3_mss1460_dir="${dir}-f3-mss1460"
for n in ${sizes}; do \
    python optackattack.py -n ${n} --bw-attacker=$bwattacker --bw-victim=$bwvictim --delay=$delay --dir=${f3_mss1460_dir} --attack-cmd="java -classpath ${curdir}/src:${curdir}/lib/* -Djava.library.path=${curdir}/rocksaw-1.0.3/lib/ ccs.neu.edu.andang.OptAckAttack" --server-cmd "${curdir}/victim-server" --sample-rate "${sample_rate}" --run-time ${time} --wscale 0 --mss 1460
done

python plot_figure3.py -s ${sizes} --template "${f2_dir}/victim_n{total}_victim{number}.txt" "${f3_mss1460_dir}/victim_n{total}_victim{number}.txt" "${f3_mss88_dir}/victim_n{total}_victim{number}.txt" --name 'mss=1460,wscale=4' 'mss=1460,wscale=0' 'mss=88,wscale=0' -o figure3.png --sample-rate 1


## Give instructions on how to view the graphs

set +e
public_IP="$(curl --connect-timeout 1 http://169.254.169.254/latest/meta-data/public-ipv4)"
set -e
if [ "${public_IP}" = "" ]; then \
    public_IP="$(hostname)"
fi

echo ""
echo "Figures saved to figure2.png and figure3.png"
echo ""
echo "To view the results in a web browser, run:"
echo "   python -m SimpleHTTPServer"
echo ""
echo "Then view these URLs in your web browser:"
echo " http://${public_IP}:8000/figure2.png"
echo " http://${public_IP}:8000/figure3.png"
echo ""
