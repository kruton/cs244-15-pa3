/* Sample TCP server */

#include <linux/tcp.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

/*
 * The following constant isn't defined in user space include files so
 * we have to define them "manually".
 */
#define TCP_CA_NAME_MAX		16

static long getmillis()
{
   struct timeval tv;
   gettimeofday(&tv, NULL);
   return (tv.tv_sec * 1000L) + (tv.tv_usec / 1000L);
}

int main(int argc, char** argv)
{
   int listenfd,connfd,n;
   struct sockaddr_in servaddr,cliaddr;
   socklen_t clilen;
   pid_t     childpid;
   char mesg[256000];
   int port;
   char *end;
   long bytes_sent;
   long current_millis, last_millis;
   long sample_millis;
   int samples_per_sec;
   struct timeval tmout;
   fd_set writefds, exceptfds;
   int optlen;
   char optval[TCP_CA_NAME_MAX];

   if (argc != 3) {
     fprintf(stderr, "Usage: %s [port] [samples/sec]\n", argv[0]);
     return 1;
   }

   port = (int) strtol(argv[1], &end, 10);
   if (end == argv[1] || *end != '\0' || errno == ERANGE) {
     fprintf(stderr, "Error: invalid port %s\n", argv[1]);
     return 1;
   }

   samples_per_sec = (int) strtol(argv[2], &end, 10);
   if (end == argv[2] || *end != '\0' || errno == ERANGE) {
     fprintf(stderr, "Error: invalid millis %s\n", argv[1]);
     return 1;
   }

   if (samples_per_sec < 1 || samples_per_sec > 10) {
     fprintf(stderr, "Error: sample rate must be between 1 and 10 inclusive"
                     "(was: %d).\n", samples_per_sec);
     return 1;
   }

   sample_millis = 1000 / samples_per_sec;

   listenfd=socket(AF_INET,SOCK_STREAM,0);

   bzero(&servaddr,sizeof(servaddr));
   servaddr.sin_family = AF_INET;
   servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
   servaddr.sin_port=htons(port);

   int reuse = 1;
   if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0)
        perror("setsockopt(SO_REUSEADDR) failed");
   bind(listenfd,(struct sockaddr *)&servaddr,sizeof(servaddr));

   if (listen(listenfd,1024) == -1) {
      perror("listen");
      return 1;
   }

   printf("Listening on port %d ...\n", port);
   memset(mesg, 'A', sizeof(mesg));

   for(;;)
   {
      clilen=sizeof(cliaddr);
      connfd = accept(listenfd,(struct sockaddr *)&cliaddr,&clilen);

      if ((childpid = fork()) == 0)
      {
         close (listenfd);
         printf("Client accepted [%d]; starting flood...\n", connfd);

         int flags = fcntl(connfd, F_GETFL, 0);
         fcntl(connfd, F_SETFL, flags | O_NONBLOCK);

         strcpy(optval, "reno");
         optlen = strlen(optval);
         if (setsockopt(connfd, IPPROTO_TCP, TCP_CONGESTION, optval, optlen) < 0) {
            perror("setsockopt");
            goto out;
         }

         last_millis = -1L;
         bytes_sent = 0;

         for(;;)
         {
            current_millis = getmillis();
            if (current_millis - last_millis > sample_millis) {
               long quantized_millis = current_millis - (current_millis % sample_millis);
               printf("status: %ld %ld\n", quantized_millis, bytes_sent);
               fflush(stdout);
               last_millis = quantized_millis;

               tmout.tv_sec = 0;
               tmout.tv_usec = (sample_millis - (current_millis % sample_millis)) * 1000L;
            }

            int ret;
            do {
               errno = 0;
               int ret = sendto(connfd,mesg,sizeof(mesg),MSG_DONTWAIT,(struct sockaddr *)&cliaddr,sizeof(cliaddr));
               if (ret == -1) {
                  if (errno != EAGAIN && errno != EWOULDBLOCK) {
                     perror("sendto");
                     goto out;
                  }
               } else {
                  bytes_sent += ret;
               }
            } while (ret > 0);

            FD_ZERO(&writefds);
            FD_ZERO(&exceptfds);
            FD_SET(connfd, &writefds);
            FD_SET(connfd, &exceptfds);

            if (select(connfd+1, NULL, &writefds, &exceptfds, &tmout) == -1) {
               perror("select");
	       goto out;
            }

            if (FD_ISSET(connfd, &exceptfds)) {
               printf("Disconnect[%d]...\n", connfd);
               goto out;
            }
         }
out:
         printf("Client finished[%d] ...\n", connfd);
      }
      close(connfd);
   }
}
